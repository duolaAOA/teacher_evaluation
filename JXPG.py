#-*-coding:utf-8-*-

import requests
import re
import time
from urllib import parse
import json

headers = {
    'Connection': 'Keep-Alive',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.8,zh-Hans-CN;q=0.5,zh-Hans;q=0.3',
    'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A403 Safari/8536.25',
    'Accept-Encoding': 'gzip, deflate',
    'Host': '10.2.0.51',
    }

head = {
    'Content-Type':'application/x-www-form-urlencoded',
    'Origin':'http://10.2.0.51',
    'Upgrade-Insecure-Requests':'1',
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
}
s = requests.session()
data = {'zjh1':'',
'tips':'',
'lx':'',
'evalue':'',
'eflag':'',
'fs':'',
'dzslh':'',
'zjh':'',
'mm':'',
'v_yzm':'',
}

#登录网址
index_url = 'http://10.2.0.51/loginAction.do'
#教师评价网址
teacher_url = 'http://10.2.0.51/jxpgXsAction.do?oper=listWj'
#评价页面网址
jxpg_url = 'http://10.2.0.51/jxpgXsAction.do'
#评价提交网址
submit_url = 'http://10.2.0.51/jxpgXsAction.do?oper=wjpg'

#提交参数
def get_paper_form_postdata(wjbm,bpr,pgnr):

    postDict = {
        'wjbm':wjbm,
        'bpr':bpr,
        'pgnr':pgnr,
        'oper':'wjShow',
        # 'wjmc':'(unable to decode value)',
        # 'bprm':'(unable to decode value)',
        # 'pgnrm':'(unable to decode value)',
        'wjbz':'null',
        'pageSize':'20',
        'page':'1',
        'currentPage':'1',
        'pageNo':''
        }


    return postDict

#最后提交参数
def submit_parse(wjbm,bpr,pgnr):

    postData = {
            'wjbm':'',
            'bpr':'',
            'pgnr':'',
            'xumanyzg':'zg',
            'wjbz':'',
            'oper':'wjpg',
            '0000000041':'4_1',
            '0000000042':'3_1',
            '0000000043':'3_1',
            '0000000044':'5_1',
            '0000000045':'5_1',
            '0000000046':'5_1',
            '0000000047':'5_1',
            '0000000048':'5_1',
            '0000000049':'5_1',
            '0000000050':'5_1',
            '0000000051':'5_1',
            '0000000052':'5_1',
            '0000000053':'5_1',
            '0000000054':'5_1',
            '0000000055':'5_1',
            '0000000056':'3_1',
            '0000000057':'4_1',
            '0000000058':'3_1',
            '0000000059':'5_1',
            '0000000060':'5_1',
            '0000000061':'5_1',
            '0000000062':'5_1',
            'zgpj':'qwerqwer'
        }


    return postData

def login():
    r = s.post(index_url, data = data,headers=headers, timeout = 20)
    teacger_html()

def teacger_html():
    r = s.post(teacher_url,headers=headers).text
    teacher_select = re.findall('<img name="(.*?)#@(.*?)#@(.*?)#@(.*?)#@(.*?)#@(.*?)"',r)
    for n in range(len(teacher_select)):
        wjbm = teacher_select[n][0]
        bpr = teacher_select[n][1]
        print('正在评价:',teacher_select[n][2],teacher_select[n][4])
        pgnr = teacher_select[n][5]

        #返回的提交参数,连接至教师评估页面
        post1 = get_paper_form_postdata(wjbm,bpr,pgnr)
        time.sleep(1)
        r = s.post(jxpg_url,data=post1,headers=headers)

        #每一位老师的评价代号
        flag = re.findall('name="(\d+)"',r.text)
        a = [flag[0]]
        temp = flag[0]
        for i in range(len(flag)):
            if temp != flag[i]:
                a.append(flag[i])
        post1 = get_paper_form_postdata(wjbm,bpr,pgnr)
        # postData = get_paper_form_postdata(wjbm,bpr,pgnr)
        #提交
        headers['Referer'] = 'http://10.2.0.51/jxpgXsAction.do?oper=listWj'
        headers['Upgrade-Insecure-Requests'] = '1'
        headers['Origin'] = 'http://10.2.0.51'
        headers['Cookie'] = 'JSESSIONID=dbcF1LCvxUSxraupg114v'
        submit = s.post(jxpg_url,data=post1,headers=headers)

        post2 = submit_parse(wjbm,bpr,pgnr)
        submit = s.post(submit_url,data=post2,headers=head)
        if '刷新一下' or '评估失败' in submit.text:
            r = s.get(jxpg_url,headers=headers)
            submit = s.post(submit_url,data=post2,headers=head)
        print('评价成功')
    print('完毕')



if __name__ == '__main__':
    # 登录
    data["zjh"] = input("请输入学号：")
    data["mm"] = input("请输入密码：")
    login()

